
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc.hpp>

#include <algorithm>
#include <chrono>
#include <cstring>
#include <dirent.h>
#include <fstream>
#include <iostream>
#include <map>
#include <math.h>
#include <streambuf>
#include <string>
#include <vector>

#include <pgm/pgm.hpp>

//------------------------------------------
//
//------------------------------------------

std::vector<int> rdList;
std::vector<float> viscList;
std::vector<float> irList;

float beginRd = 600.0f;
float endRd = 2400.0f;

float beginVisc = 1.0f;
float endVisc = 10.0f;

float beginIr = 0.042f;
float endIr = 0.06f;

//------------------------------------------
//
//------------------------------------------

struct Instant{
    int bestSimulation;
    float bestDistance;
    std::vector<int> simulations;
    int visc;
    int ir;
    int rd;
};

//------------------------------------------
//
//------------------------------------------
float dot(float ax, float ay, float bx, float by){
    return ax * bx + ay * by;
}

float length(float ax, float ay){
    return sqrtf(dot(ax, ay, ax, ay));
}

float euclideanDistance(cv::Mat capture, cv::Mat simu){

    float distance = 0;

    for(int x = 0; x < capture.cols; x++){

        for(int y = 0; y < capture.rows; y++){

            /*
            int ic = int(capture->getPixel(x, y));
            int is = int(simu->getPixel(x, y));
            */

            int ic = int(capture.at<uchar>(y,x));
            int is = int(simu.at<uchar>(y,x));

            //if(ic != 0 || is != 0)
              //  printf("x, y, ic, is : %d ; %d ; %d ; %d \n", x, y, ic, is);

            float c = 0;
            float s = 0;

            c = float(ic) / 255.0f;
            s = float(is) / 255.0f;

            //if(c > 0.0f || s > 0.0f){
                distance += ((c - s) * (c - s));
            //}

        }

    }

    return distance;

}

int getSimulationIndex(std::string directoryName){

    int indexRd = -1;
    int indexVisc = -1;
    int indexIr = -1;

    std::string tmp = "";
    bool getValue = false;

    for(int i = 0; i < directoryName.length(); i++){

        if(directoryName.at(i) == '_'){

            if(tmp == ""){
                getValue = !getValue;
            }
            else{
                if(indexRd == -1){
                    indexRd = std::find(rdList.begin(), rdList.end(), std::atoi(tmp.c_str())) - rdList.begin();
                }
                else if(indexVisc == -1){
                    for(int e = 0; e < viscList.size(); e++){
                        if(int(std::atof(tmp.c_str())*10000) == int(viscList[e]*10000)){
                            indexVisc = e;
                            break;
                        }
                    }
                }
                tmp = "";
                getValue = !getValue;
            }

        }
        else if(getValue){
            tmp += directoryName.at(i);
        }

    }

    if(getValue){
        for(int e = 0; e < irList.size(); e++){
            if(int(std::atof(tmp.c_str()) * 100000) == int(irList[e] * 100000)){
                indexIr = e;
                break;
            }
        }
    }

    int indexSimu = indexIr + indexVisc * irList.size() + indexRd * irList.size() * viscList.size();

    return indexSimu;

}

//------------------------------------------
//
//------------------------------------------
int main(int argc, char** argv ){

    std::string simulationPath = "donnees";
    std::string capturePath = "donnees/rd1/";

    int minImage = 0;
    int maxImage = 300;

    //float seuil = 512.0f*512.0f/256.0f;
    //float seuil = 1000.0f;
    float seuil = 5000.0f;

    //if the simulation path or the capture path we can't compare the images
    if(simulationPath == "" || capturePath == ""){
        printf("simulationPath and capturePath needs to be defined \n");
        return 0;
    }
    else {

        printf("simulationPath : %s \n", simulationPath.c_str());
        printf("capturePath : %s \n", capturePath.c_str());
        printf("minImage, maxImage : %d ; %d \n", minImage, maxImage);

        cv::Mat capture;
        cv::Mat simu;

        std::string captureFile = "";
        std::string simulationFile = "";

        float distance = 0.0f;

        rdList = {998, 1100, 1200, 1300, 1400, 1500, 1600, 1700, 1800, 1900, 2000};
        viscList = {0.01f, 0.1f, 0.2f, 0.3f, 0.4f, 0.5f, 0.6f, 0.7f, 0.8f, 0.9f, 1.0f};
        irList = {0.042f, 0.044f, 0.046f, 0.048f, 0.050f, 0.052f, 0.054f, 0.056f, 0.058f, 0.060f};

        auto start = std::chrono::high_resolution_clock::now();

        //get all simulations
        DIR *dir;
        struct dirent *ent;

        DIR *subdir;
        struct dirent *subent;

        std::map<int, int> mapStable;
        std::vector<int> simuIndexList;

        dir = opendir(simulationPath.c_str());

        int index = 1;
        while( (ent = readdir(dir)) != NULL ){
            if(ent->d_name[0] == 'r' && ent->d_name[1] == 'd'){
            
                subdir = opendir(("donnees/rd"+std::to_string(index)+"/").c_str());
                int countImg = 0;

                while( (subent = readdir(subdir)) != NULL){
                    if(subent->d_name[0] == 'r')
                        countImg++;
                }

                int indexSimu = index;

                printf("%s : %d ; %d \n", ent->d_name, indexSimu, countImg);

                mapStable.insert(std::pair<int, int>(indexSimu, countImg));
                simuIndexList.push_back(indexSimu);

                closedir(subdir);
                index++;
            }
        }

        closedir(dir);

     //   if(simuIndexList.size() < irList.size() * viscList.size() * rdList.size()){
     //       printf("---- Il manque des simulations \n");
     //   }

        dir = opendir(capturePath.c_str());
        int countCapture = 0;

        while( (ent = readdir(dir)) != NULL){
            if(ent->d_name[0] == 'r')
                countCapture++;
        }

        printf("nb capture : %d \n", countCapture);

        printf("---------------------------------\n");

        closedir(dir);

        std::map<int, float> mapDistance;

        /*
        std::map<int, int> mapIndice;
        std::map<int, int> mapRd;
        std::map<int, int> mapVisc;
        std::map<int, int> mapIr;
        */

        std::map<int, Instant> mapInstant;
        
        FILE *f = fopen("results.txt","w");
        fprintf(f,"instant ; meilleure_simulation ; rd ; viscosite ; ir ; distance \n");
        
        for(int image = 1; image <= countCapture; image++){

            captureFile = capturePath+"result"+std::to_string(countCapture - image)+".pgm";
            //capture = new PGMBitmap(captureFile.c_str());
            capture = cv::imread(captureFile.c_str(), CV_LOAD_IMAGE_GRAYSCALE);
            printf("nbSimu:%d\n",simuIndexList.size());
            for(int s = 0; s < simuIndexList.size(); s++){

                int indiceSimu = s+1;
                int stableIndice = mapStable.find(indiceSimu)->second;
                //printf("stableIndice:%d\n",stableIndice);
                
                if(stableIndice >= image && stableIndice != 0){

                    std::string directory = "/rd"+std::to_string(indiceSimu)+"/";
                    //printf("directory:%s\n",directory.c_str());
                    simulationFile = simulationPath+directory+"result"+std::to_string(stableIndice - image)+".pgm";
                    //printf("%s",simulationFile.c_str());
                    simu = cv::imread(simulationFile.c_str(), CV_LOAD_IMAGE_GRAYSCALE);
                    distance = euclideanDistance(capture, simu);
                    mapDistance.insert(std::pair<int, float>(indiceSimu, distance));
                }
                else{
                    mapDistance.insert(std::pair<int, float>(indiceSimu, -1.0f));
                }
            }
            
            Instant currentInstant;
            currentInstant.bestSimulation = -1;
            currentInstant.bestDistance = -1;

            // -- Important -- //
            // On trouve dMin afin de garder la simulation dont la distance est égale à dMin
            float dMin = 100000000;
            for(int indiceSimulation = 0; indiceSimulation < simuIndexList.size(); indiceSimulation++){
                distance = mapDistance.find(simuIndexList[indiceSimulation])->second;
               // printf("distance:%f\n",distance);
                if(distance <= seuil && distance >= 0.0f){
                    if(distance<=dMin) dMin = distance;
                }
            }
            // --  -- //
            printf("dMin:%f\n",dMin);

            printf("instant:%d ",countCapture-image);
            for(int indiceSimulation = 0; indiceSimulation < simuIndexList.size(); indiceSimulation++){

                distance = mapDistance.find(simuIndexList[indiceSimulation])->second;
                // -- Important -- //
                // On ne garde que la simu dont la distance est inférieure ou égale à dMin
                 // -- Important -- //
                
                if(distance <= seuil && distance <= dMin && distance>=0.0f){
                        currentInstant.bestSimulation = simuIndexList[indiceSimulation];
                        //currentInstant.rd = currentInstant.bestSimulation/(irList.size()*viscList.size());
                        //currentInstant.visc = (currentInstant.bestSimulation%(irList.size()*viscList.size()))/(irList.size());
                        //currentInstant.ir = currentInstant.bestSimulation%(irList.size());
                        //float rd = rdList[currentInstant.rd];
                        //float visc = viscList[currentInstant.visc];
                        //float ir = irList[currentInstant.ir];
                    
                        fprintf(f,"%d %d\n",countCapture- image,indiceSimulation+1);
                        printf("%d\n",indiceSimulation+1);
                    }
            }
            mapDistance.clear();
            //delete capture;
        }
        fclose(f);

//std::chrono::duration<double> duration = end - start;

       // printf("time execution : %f \n", duration.count());

    }
}
